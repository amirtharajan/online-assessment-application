from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('assessment_service.urls.common')),
    path('faculty/', include('assessment_service.urls.faculty')),
    path('admin/', include('assessment_service.urls.admin')),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)