from django.contrib.auth.models import auth
from django.shortcuts import render, redirect
from django.contrib import messages

from assessment_service.models import Login
from assessment_service.model.FacultyModel import Faculty

from assessment_service.helpers import make_hash
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from datetime import datetime


def admin(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    search = ''
    if 'search' in request.GET:
        search = request.GET['search']
        users = Login.objects.filter(first_name__contains=search).order_by('-created_at')
    else:
        users = Login.objects.order_by('-created_at')

    paginator = Paginator(users, 12)
    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    return render(request, 'admin.html', {'users':users, 'search':search,'user_id':request.user.id})

def get_faculty_details(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    faculty_detail = Faculty.objects.filter(login_id=request.GET['faculty_details_id'])

    if not faculty_detail:
        messages.info(request, 'Details Not Found for uset ID {}'.format(request.GET['faculty_details_id']))
        return redirect('admin')

    return render(request, 'faculty_details.html', {'faculty_detail': faculty_detail, 'login_id':faculty_detail[0].login_id})

def update_faculty_details(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    if 'id' not in request.POST:
        return redirect('admin')
 
    faculty = Faculty.objects.filter(id=request.POST['id']).first()

    faculty.allotted_department = request.POST['department'] if request.POST['department'] else faculty.allotted_department
    faculty.allotted_subject_code = request.POST['subject_code'] if request.POST['subject_code'] else faculty.allotted_subject_code
    faculty.allotted_subject_name = request.POST['subject_name'] if request.POST['subject_name'] else faculty.allotted_subject_name
    faculty.year = request.POST['year'] if request.POST['year'] else faculty.year
    faculty.section = request.POST['section'] if request.POST['section'] else faculty.section
    faculty.updated_at = datetime.now()
    faculty.save()
    messages.success(request, 'Successfully Updated')

    faculty_detail = Faculty.objects.filter(login_id=request.POST['login_id'])

    if not faculty_detail:
        return redirect('admin')

    return render(request, 'faculty_details.html', {'faculty_detail': faculty_detail, 'login_id': faculty_detail[0].login_id})

def delete_faculty_details(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    Faculty.objects.filter(id=request.GET['delete_faculty_id']).delete()
    messages.success(request, 'Successfully Deleted')

    faculty_detail = Faculty.objects.filter(login_id=request.GET['login_id'])

    if not faculty_detail:
        return redirect('admin')

    return render(request, 'faculty_details.html', {'faculty_detail': faculty_detail, 'login_id':faculty_detail[0].login_id})

def create_user(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    new_login = Login(
        username=request.POST['username'],
        password=make_hash(request.POST['password']),
        role=request.POST['role'],
        active=True
    )
    new_login.save()
    messages.success(request, 'New user created successfully')
    return redirect('admin')

def delete_user(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    Login.objects.filter(id=request.GET['delete_user_id']).delete()
    messages.success(request, 'User deleted successfully')
    return redirect('admin')

def activate(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    login = Login.objects.filter(id=request.GET['active_user_id']).first()

    if login.active:
        login.active = False
        login.save()
        messages.success(request, 'Accound BLOCKED successfully')
        return redirect('admin')

    login.active = True
    login.save()
    messages.success(request, 'Account UNBLOCKED successfully')
    return redirect('admin')

def add_subject(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.user.role != 'admin':
        messages.warning(request, 'You dont have the permission to access the ADMIN page')
        return redirect('home')

    if 'new_subject' in request.POST:
        check = Faculty.objects.filter(
            login_id=request.POST['login_id'],
            allotted_department=request.POST['department'].upper(),
            allotted_subject_code=request.POST['allotted_subject_code'].upper(),
            year=request.POST['year'],
            section=request.POST['section'].upper(),
        ).first()
        if check:
            messages.warning(request, 'Subject code {}, Department {}, Year {}, Section {},  is already allocated to this user'.format(
                request.POST['allotted_subject_code'],
                request.POST['department'],
                request.POST['year'],
                request.POST['section'],
            ))
            return redirect('admin')

    subject_code = request.POST['allotted_subject_code'].upper()
    remove_char_list = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '-', '`', '~', '{', '}', '[', ']', ':', ';', '"', "'", ',', '.', '<','>','=','/','?','|',' ']
    valid_subject_code = ''
    for d in subject_code:
        if d not in remove_char_list:
            valid_subject_code += d
    Faculty(
        login_id=request.POST['login_id'],
        allotted_department=request.POST['department'].upper(),
        allotted_subject_code=valid_subject_code,
        allotted_subject_name=request.POST['allotted_subject_name'].capitalize() ,
        year=request.POST['year'],
        section=request.POST['section'].upper(),
    ).save()
    messages.success(request, 'Subject code {} is successfully allocated to user'.format(request.POST['allotted_subject_code']))
    return redirect('admin')
