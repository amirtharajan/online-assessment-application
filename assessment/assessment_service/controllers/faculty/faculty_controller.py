from django.contrib import messages
from django.shortcuts import render, redirect

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from assessment_service.model.AssessmentModel import Assessment
from assessment_service.model.FacultyModel import Faculty
from assessment_service.model.QuestionModel import Question
from assessment_service.model.CartModel import Cart
from assessment_service.model.StudentModel import Student

from assessment_service.helpers import (
    generate_text_key,
    end_time,
    split_questions,
    excel_generator,
)

import re



def faculty(request):
    if not request.user.is_authenticated:
        return redirect('login')

    return render(request, 'faculty.html')


def select_question_paper(request):
    if not request.user.is_authenticated:
        return redirect('login')

    return render(request, 'option.html')


def init_new_question_paper(request):
    if not request.user.is_authenticated:
        return redirect('login')

    faculty_details = Faculty.objects.filter(login_id=request.user.id)
    subjects = []
    for data in faculty_details:
        subjects.append(data.allotted_subject_code +
                        ' - ' + data.allotted_subject_name)

    return render(request, 'question.html', {'subjects': subjects})


def create_new_question(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if 'question_image' in request.FILES:
        question_image = request.FILES['question_image']
    else:
        question_image = None

    faculty_details = Faculty.objects.filter(login_id=request.user.id)
    subjects = []
    for data in faculty_details:
        subjects.append(data.allotted_subject_code +
                        ' - ' + data.allotted_subject_name)

    if request.method == 'POST':
        choices = [
            'A : ' + request.POST['a'].capitalize(),
            'B : ' + request.POST['b'].capitalize(),
        ]

        answers = ['A', 'B']
        if request.POST['c'] and request.POST['c'] != '':
            choices.append('C : ' + request.POST['c'].capitalize())
            answers.append('C')
        if request.POST['d'] and request.POST['d'] != '':
            choices.append('D : ' + request.POST['d'].capitalize())
            answers.append('D')
        if request.POST['e'] and request.POST['e'] != '':
            choices.append('E : ' + request.POST['e'].capitalize())
            answers.append('E')
        if request.POST['f'] and request.POST['f'] != '':
            choices.append('F : ' + request.POST['f'].capitalize())
            answers.append('F')

        answer = request.POST['answer'].upper()
        char_list = ['[', '{', "'", '"', '}', ']', ',']
        answer = re.sub("|".join(char_list), '', answer)

        for d in answer:
            if d not in answers:
                messages.error(request, 'Invalid answer')
                return render(request, 'question.html', {
                    'subject_code': request.POST['subject_code'],
                    'question_text': request.POST['question_text'] if request.POST['question_text'] else None,
                    'a': request.POST['a'],
                    'b': request.POST['b'],
                    'c': request.POST['c'] if request.POST['c'] else '',
                    'd': request.POST['d'] if request.POST['d'] else '',
                    'e': request.POST['e'] if request.POST['e'] else '',
                    'f': request.POST['f'] if request.POST['f'] else '',
                    'answer': request.POST['answer'],
                })

        new_question = Question(
            subject_code=request.POST['subject_code'],
            question_text=request.POST['question_text'] if request.POST['question_text'] else None,
            question_image=question_image,
            choices=choices,
            answer=answer,
            type='single' if len(answer) == 1 else 'multiple',
        )
        new_question.save()
        messages.success(request, 'New Question added successfully')
        return redirect('init_new_question_paper')

    return render(request, 'question.html', {'subjects': subjects})


def list_questions(request):
    if not request.user.is_authenticated:
        return redirect('login')

    faculty_details = Faculty.objects.filter(login_id=request.user.id)
    cart = Cart.objects.filter(login_id=request.user.id)
    subjects = []
    for data in faculty_details:
        subjects.append(data.allotted_subject_code +
                        ' - ' + data.allotted_subject_name)

    subject_code = ''
    if 'add_cart' in request.GET:
        check = Cart.objects.filter(
            question_id=request.GET['add_cart']).first()
        subject = Question.objects.filter(id=request.GET['add_cart']).first()
        subject_code = subject.subject_code
        if check:
            messages.info(request, 'Already Added')

        else:
            check_confilict = Cart.objects.filter().first()
            if check_confilict:
                d = Question.objects.filter(
                    id=check_confilict.question_id).first()
                if subject.subject_code == d.subject_code:
                    new_cart = Cart(
                        login_id=request.user.id,
                        question_id=request.GET['add_cart']
                    )
                    new_cart.save()
                    messages.success(request, 'Successfully added into Cart')
                else:
                    messages.warning(
                        request, 'Two subject Questions can not be added in same cart')
            else:
                new_cart = Cart(
                    login_id=request.user.id,
                    question_id=request.GET['add_cart']
                )
                new_cart.save()
                messages.success(request, 'Successfully added into Cart')

    elif 'delete_question_id' in request.GET:
        d = Question.objects.filter(
            id=request.GET['delete_question_id']).first()
        Cart.objects.filter(
            question_id=request.GET['delete_question_id']).delete()
        subject_code = d.subject_code
        d.delete()
        messages.success(request, 'Question deleted Successfully')

    elif 'subject_code' in request.GET:
        subject_code = request.GET['subject_code']

    questions = Question.objects.filter(
        subject_code=subject_code).order_by('-created_at')

    paginator = Paginator(questions, 10)
    page = request.GET.get('page')
    try:
        questions = paginator.page(page)
    except PageNotAnInteger:
        questions = paginator.page(1)
    except EmptyPage:
        questions = paginator.page(paginator.num_pages)

    for d in questions:
        d.choices = split_questions(d.choices)

    return render(request, 'list_questions.html', {'questions': questions, 'subject_code': subject_code, 'subjects': subjects, 'cart_total': len(cart)})


def get_cart(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if 'remove_cart_item_id' in request.GET:
        Cart.objects.filter(
            question_id=request.GET['remove_cart_item_id']).delete()
        messages.success(request, 'Question ID {} has been removed from your cart'.format(
            request.GET['remove_cart_item_id']))
        return redirect('get_cart')

    elif 'update_answer' in request.GET:
        answer = request.GET['update_answer'].upper()
        char_list = ['[', '{', "'", '"', '}', ']', ',']
        answer = re.sub("|".join(char_list), '', answer)
        Question.objects.filter(id=request.GET['update_answer_id']).update(
            answer=answer, type='single' if len(answer) == 1 else 'multiple'
        )
        messages.success(request, 'Question ID {} Answer updated'.format(
            request.GET['update_answer_id']))
        return redirect('get_cart')

    question_ids = Cart.objects.filter(
        login_id=request.user.id).values_list('question_id', flat=True)

    if not question_ids:
        messages.info(request, 'Your Cart is empty')
        return redirect('faculty')

    questions = Question.objects.filter(id__in=question_ids)
    for d in questions:
        d.choices = split_questions(d.choices)
    return render(request, 'cart.html', {'questions': questions, 'subject_code': questions[0].subject_code, 'total': len(questions)})


def get_previous_question_paper(request):
    if not request.user.is_authenticated:
        return redirect('login')

    assessment_details = Assessment.objects.filter(
        assessment_key=request.GET['previous_test_key']).first()
    if not assessment_details:
        messages.error(request, 'Key not found')
        return render(request, 'question.html')

    questions = split_questions(assessment_details.questions)
    return render(request, 'question.html', {'assessment_details': assessment_details, 'total_questions': len(questions)})


def create_assessment(request):
    if not request.user.is_authenticated:
        return redirect('login')

    key = generate_text_key()
    check = Assessment.objects.filter(assessment_key=key).first()
    if check:
        key = generate_text_key()

    new_assessment = Assessment(
        assessment_key=key,
        login_id=request.user.id,
        assessment_title=request.POST['title'].capitalize(),
        date=request.POST['assessment_date'],
        start_time=request.POST['start_time'],
        end_time=end_time(request.POST['start_time'],
                          request.POST['duration_in_minutes']),
        duration_in_minutes=request.POST['duration_in_minutes'],
        total_questions=request.POST['total_questions'],
        total_marks=request.POST['total_marks'],
        pass_mark=request.POST['pass_mark'],
        display_result=True if 'display_result' in request.POST and request.POST[
            'display_result'] == 'on' else False,
    )
    new_assessment.save()
    messages.success(
        request, 'Note down your Assessment key - {} '.format(key))
    return redirect('select_question_paper')


def generate_question_paper(request):
    if not request.user.is_authenticated:
        return redirect('login')

    question_ids = Cart.objects.filter(
        login_id=request.user.id).values_list('question_id', flat=True)
    if not question_ids:
        messages.info(request, 'Your cart is empty')
        return redirect('faculty')

    if 'test_key' in request.POST:
        assessment = Assessment.objects.filter(
            assessment_key=request.POST['test_key']).first()
        if not assessment:
            messages.error(request, 'Invalid Assessment or Test key')
            return render(request, 'confirmation.html', {'key': request.POST['test_key']})

        questions = []
        for d in question_ids:
            questions.append(d)

        if assessment.total_questions > len(questions):
            messages.warning(request, 'Assessment ID {}, Expected minimum no of Questions {}, but got only {}'.format(
                assessment.id, assessment.total_questions, len(questions)
            ))
            return render(request, 'confirmation.html', {'key': request.POST['test_key']})

        assessment.questions = str(questions)
        assessment.save()
        Cart.objects.filter(login_id=request.user.id).delete()
        messages.success(
            request, 'Success, Question paper successfully Generated')
        return render(request, 'final.html', {'assessment': assessment})

    return render(request, 'confirmation.html')


def get_my_assessments(request):
    if not request.user.is_authenticated:
        return redirect('login')

    my_assessment_details = Assessment.objects.filter(
        login_id=request.user.id).order_by('-updated_at')

    if not my_assessment_details:
        messages.info(request, 'You dont have any assessment data')
        return redirect('faculty')

    return render(request, 'list_assessments.html', {'my_assessment_details': my_assessment_details, 'total_assessments': len(my_assessment_details)})


def delete_my_assessment(request):
    if not request.user.is_authenticated:
        return redirect('login')

    Assessment.objects.filter(id=request.GET['delete_assessment_id']).delete()

    messages.success(request, 'Assessment deleted successfully')
    return redirect('get_my_assessments')


def get_result(request):
    if not request.user.is_authenticated:
        return redirect('login')

    result = Student.objects.filter(assessment_key=request.GET['key']).order_by('register_number')
    if not result:
        messages.info(request, 'Result data not found for Assessment ID {}'.format(request.GET['key']))
        return redirect('get_my_assessments')

    if 'excel' in request.POST:
        return excel_generator(result, request.GET['key'])

    assessment = Assessment.objects.filter(assessment_key=request.GET['key']).first()
    return render(request, 'result.html', {'result': result, 'assessment':assessment})

def second_chance(request):
    print('hi')
    if 'second_chance' in request.GET:
        Student.objects.filter(id=request.GET['second_chance']).update(
            attempts=1,
            attended_questions=0,
            score=0,
            is_pass=False,
            updated_at=None
        )
    messages.success(request, 'Reseted')
    return redirect('/faculty/result?key={}'.format(request.GET['key']))