from django.contrib.auth.models import auth
from django.shortcuts import render, redirect
from django.contrib import messages

from assessment_service.models import Login
from assessment_service.model.LoginHistoryModel import LoginHistory
from assessment_service.model.AssessmentModel import Assessment
from assessment_service.model.QuestionModel import Question
from assessment_service.model.StudentModel import Student

from datetime import datetime
import random

from assessment_service.helpers import validate_assessment, split_questions, split_choices



def home(request):
    return render(request, 'home.html')

def instruction(request):
    assessment = Assessment.objects.filter(assessment_key=request.POST['test_key']).first()
    if not assessment:
        messages.error(request, 'Invalid Key')
        return render(request, 'home.html', {'test_key': request.POST['test_key']})

    is_valid = validate_assessment(assessment)
    print(is_valid)
    if not is_valid:
        messages.error(request, 'Invalid Key or Expired')
        return redirect('home')

    if 'student_details' in request.POST:
        check = Student.objects.filter(
            assessment_key=request.POST['test_key'],
            email=request.POST['email'].lower(),
            register_number=request.POST['register_number']
            ).first()
        if check:
            if check.attempts >= 4:
                messages.error(request, 'You cannot take an assessment or test')
                return redirect('home')

            request.session['test_session'] = str(check.id) + '+' + check.assessment_key
            return render(request, 'instructions.html', {'assessment': assessment, 'student_id':check.id})

        else:
            email = request.POST['email'].split('@')
            role_number = email[0].lower()
            if not role_number[0].isdigit() or not role_number[-1].isdigit() or email[-1] != 'kpriet.ac.in' and email[-1] != 'kprcas.ac.in':
                messages.error(request, 'Invalid Email. College Email only allowed')
                return render(request, 'instructions.html', {
                    'test_key':request.POST['test_key'],
                    'full_name':request.POST['full_name'],
                    'email': request.POST['email'],
                    'register_number':request.POST['register_number'],
                    'department':request.POST['department'],
                    'year':request.POST['year'],
                    'section':request.POST['section'],
                })

            if not request.POST['register_number'].isdigit() :
                messages.error(request, 'Invalid Register Number')
                return render(request, 'instructions.html', {
                    'test_key':request.POST['test_key'],
                    'full_name':request.POST['full_name'],
                    'email': request.POST['email'],
                    'register_number':request.POST['register_number'],
                    'department':request.POST['department'],
                    'year':request.POST['year'],
                    'section':request.POST['section'],
                })

            new_student = Student(
                assessment_key=request.POST['test_key'],
                full_name=request.POST['full_name'],
                email=request.POST['email'],
                role_number=role_number,
                register_number=request.POST['register_number'],
                department=request.POST['department'],
                year=request.POST['year'],
                section=request.POST['section'],
            )
            new_student.save()
            request.session['test_session'] = str(new_student.id) + '+' + request.POST['test_key']
            return render(request, 'instructions.html', {'assessment': assessment, 'student_id': new_student.id})

    return render(request, 'instructions.html', {'test_key':request.POST['test_key']})

def login(request):
    if request.user.is_authenticated:
        return redirect('faculty')

    elif request.method == 'POST':
        check = Login.objects.filter(username=request.POST['username']).first()
        if not check:
            messages.error(request, 'Username or Email id not found')
            return render(request, 'login.html', {'username': request.POST['username']})

        if not check.active:
            messages.error(request, 'Your account has been blocked')
            return render(request, 'login.html', {'username': request.POST['username']})

        login = auth.authenticate(username=request.POST['username'], password=request.POST['password'])
        if not login:
            messages.error(request, 'Sorry, Given password is wrong')
            return render(request, 'login.html', {'username':request.POST['username']})

        auth.login(request, login)

        new_login = LoginHistory(
            login_id=login.id,
            login_at=datetime.now(),
        )
        new_login.save()
        messages.success(request, 'Welcome {}'.format(login.first_name))
        return render(request, 'home.html')

    return render(request, 'login.html')

def logout(request):
    auth.logout(request)
    return redirect('home')

def take_test(request):
    if 'test_session' not in request.session:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    check = Student.objects.filter(id=request.GET['student_test_id']).first()
    if not check:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    if request.session['test_session'] != str(check.id) + '+' + check.assessment_key:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    if 'page' not in request.GET:
        if check.attempts >= 4:
            messages.error(request, 'You cannot take an assessment or test')
            return redirect('home')

        check.attempts += 1
        check.save()

    assessment = Assessment.objects.filter(assessment_key=request.GET['key']).first()
    if not assessment:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    data = split_questions(assessment.questions)
    question_data = Question.objects.filter(id__in=data)
    question = []
    for d in question_data:
        question.append(d)

    random.shuffle(question)
    questions = []
    increments = split_choices(d.choices)
    choices = split_questions(d.choices)
    options = []
    for increment, choice in zip(increments, choices):
        options.append({'increment':increment, 'choice':choice})
    for no, d in enumerate(question):
        data = {
            'no': no + 1,
            'id': d.id,
            'subject_code': d.subject_code,
            'question_text': d.question_text,
            'question_image': d.question_image if d.question_image else None,
            'choices': options,
            'type': d.type,
        }
        questions.append(data)
    if 'page' not in request.GET:
        messages.success(request, 'Mr / Miss / Mrs : {}, ALL THE BEST'.format(check.full_name))

    print(questions[0:assessment.total_questions])
    return render(request, 'test.html', {
        'student_id': check.id,
        'full_name': check.full_name,
        'questions': questions[0:assessment.total_questions],
        'question_data': question_data,
        'student_test_id': request.GET['student_test_id'],
        'key': request.GET['key'],
        'title': assessment.assessment_title,
        'date': assessment.date,
        'end_time': assessment.end_time,
        'count_down_time': str(assessment.end_time)
    })

def submit_test(request):
    if 'test_session' not in request.session:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    student = Student.objects.filter(id=request.POST['student_id']).first()
    if request.session['test_session'] != str(student.id) + '+' + student.assessment_key:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    assessment_data = Assessment.objects.filter(assessment_key=request.POST['assessment_key']).first()
    question_ids = split_questions(assessment_data.questions)
    question_data = Question.objects.filter(id__in=question_ids)

    score = 0
    attended_questions = 0
    for question_id in question_ids:
        if question_id in request.POST:
            data = request.POST.getlist(question_id)
            user_answer = ''
            for d in data:
                user_answer += d

            for question in question_data:
                if question.id == int(question_id):
                    if user_answer == question.answer:
                        score += 1
                        attended_questions += 1
                    else:
                        attended_questions += 1

    student.score += score
    student.attended_questions += attended_questions
    student.updated_at = datetime.now()

    assessment_data.total_students_attended += 1

    if score >= assessment_data.pass_mark:
        assessment_data.no_of_pass += 1
        student.is_pass = True
    else:
        assessment_data.no_of_fail += 1
        student.is_pass = False

    student.save()
    assessment_data.save()

    messages.success(request, 'Thank you {}'.format(student.full_name))
    del request.session['test_session']
    return redirect('/test-end?id={}'.format(student.id))

def test_end(request):
    if 'test_session' in request.session:
        messages.error(request, 'Forbidden or Unauthorized access')
        return redirect('home')

    student = Student.objects.filter(id=request.GET['id']).first()
    assessment_data = Assessment.objects.filter(assessment_key=student.assessment_key).first()
    return render(request, 'test_end.html', {
        'full_name': student.full_name,
        'score': student.score if assessment_data.display_result else None,
        'total': assessment_data.total_marks,
        'result': True if student.score >= assessment_data.pass_mark else False,
    })