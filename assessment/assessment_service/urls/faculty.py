from django.urls import path

from assessment_service.controllers.faculty.faculty_controller import (
    faculty,
    create_assessment,
    select_question_paper,
    init_new_question_paper,
    get_previous_question_paper,
    create_new_question,
    list_questions,
    get_cart,
    generate_question_paper,
    get_my_assessments,
    delete_my_assessment,
    get_result,
    second_chance,
)


urlpatterns = [
    path('', faculty, name='faculty'),
    path('assessment', create_assessment, name='create_assessment'),
    path('question-paper', select_question_paper, name='select_question_paper'),
    path('init-new-question-paper', init_new_question_paper, name='init_new_question_paper'),
    path('get-previous-question-paper', get_previous_question_paper, name='get_previous_question_paper'),
    path('create-question', create_new_question, name='create_new_question'),
    path('list-questions', list_questions, name='list_questions'),
    path('cart-items', get_cart, name='get_cart'),
    path('confirmation', generate_question_paper, name='generate_question_paper'),
    path('my-assessments', get_my_assessments, name='get_my_assessments'),
    path('delete-assessment', delete_my_assessment, name='delete_my_assessment'),
    path('result', get_result, name='result'),
    path('chance', second_chance, name='chance'),
]
