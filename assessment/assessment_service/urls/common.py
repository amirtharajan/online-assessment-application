from django.urls import path

from assessment_service.controllers.common.common_controller import (
    home,
    instruction,
    login,
    logout,
    take_test,
    submit_test,
    test_end,
)


urlpatterns = [
    path('', home, name='home'),
    path('instruction', instruction, name='instruction'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('test', take_test, name='take_test'),
    path('submit-test', submit_test, name='submit_test'),
    path('test-end', test_end, name='test_end'),

]
