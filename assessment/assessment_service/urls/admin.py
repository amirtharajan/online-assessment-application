from django.urls import path

from assessment_service.controllers.admin.admin_controller import (
    admin,
    create_user,
    activate,
    get_faculty_details,
    update_faculty_details,
    delete_faculty_details,
    delete_user,
    add_subject,
)


urlpatterns = [
    path('', admin, name='admin'),
    path('new-user', create_user, name='create_user'),
    path('activate', activate, name='activate'),
    path('more-details', get_faculty_details, name='get_faculty_details'),
    path('delete-faculty-details', delete_faculty_details, name='delete_faculty_details'),
    path('update-faculty-details', update_faculty_details, name='update_faculty_details'),
    path('delete-user', delete_user, name='delete_user'),
    path('add-subject', add_subject, name='add_subject'),
]
