from django.conf import settings
from django.contrib.auth.hashers import make_password

import xlwt
from django.http import HttpResponse

from datetime import timedelta, date, datetime, time

SALT = settings.SALT
HASHER = settings.HASHER

import uuid
import re


def make_hash(user_password):
    hash_password = make_password(user_password, salt=SALT, hasher=HASHER)
    return hash_password

def generate_text_key(string_length=10):
    random = str(uuid.uuid4())
    random = random.upper()
    random = random.replace("-", "")
    return random[0:8]

def end_time(start_time, duration):
    t = start_time.split(':')
    h = int(t[0]) * 60
    t = h + int(t[1])
    duration = timedelta(minutes=int(duration))
    end_time = timedelta(minutes=t)
    end_time = duration + end_time
    return str(end_time)

def assessment_end_time(start_time, duration):
    t = start_time.split(':')
    h = int(t[0]) * 60
    t = h + int(t[1])
    duration = timedelta(minutes=int(duration))
    end_time = timedelta(minutes=t)
    end_time = end_time - duration
    end_time = datetime.strptime(str(end_time), '%H:%M:%S').time()
    return end_time

def split_questions(items):
    char_list = ['[', '{', "'", '"', '}', ']']
    items = re.sub("|".join(char_list), "", items)
    items = items.replace('[', '')
    items = items.replace(']', '')
    items = items.split(', ')
    return items

def validate_assessment(assessment):
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_time = datetime.strptime(current_time, '%H:%M:%S').time()
    assessment.end_time = assessment_end_time(str(assessment.end_time), int(assessment.duration_in_minutes)//2)
    print(assessment.date != date.today(),'*', date.today())
    if assessment.date != date.today():
        return False

    if current_time >= assessment.start_time or current_time <= assessment.end_time:
        return True

    return False

def split_choices(items):
    char_list = ['[', '{', "'", '"', '}', ']']
    items = re.sub("|".join(char_list), "", items)
    items = items.replace('[', '')
    items = items.replace(']', '')
    items = items.split(', ')
    choices = []
    for d in items:
        choices.append(d[0])
    return choices

def excel_generator(result, key):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ID_{}_result.xls"'.format(key)
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(key)
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = [
        'Student Name',
        'Score',
        'Is Pass?',
        'Email',
        'Role Number',
        'Register_number',
        'Department',
        'Year',
        'Section',
        'Attended Questions',
        'Submited At',
    ]

    row_num = 0
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    date_format = "%Y-%m-%d %H:%M:%S" 
    for data in result:
        result = 'Pass' if data.is_pass else 'Fail'
        row_num = row_num + 1
        ws.write(row_num, 0, data.full_name, font_style)
        ws.write(row_num, 1, data.score, font_style)
        ws.write(row_num, 2, result, font_style)
        ws.write(row_num, 3, data.email, font_style)
        ws.write(row_num, 4, data.role_number, font_style)
        ws.write(row_num, 5, data.register_number, font_style)
        ws.write(row_num, 6, data.department, font_style)
        ws.write(row_num, 7, data.year, font_style)
        ws.write(row_num, 8, data.section, font_style)
        ws.write(row_num, 9, data.attended_questions, font_style)
        ws.write(row_num, 10, datetime.strftime(data.updated_at, date_format), font_style)
    wb.save(response)
    return response