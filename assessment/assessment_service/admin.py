from django.contrib import admin
from assessment_service.models import Login
from assessment_service.model.FacultyModel import Faculty

admin.site.site_header = 'KPR'
admin.site.index_title = 'KPR-ADMIN'
admin.site.site_title = 'KPR-MAIN'

class LoginAdmin(admin.ModelAdmin):
    list_display = ['username', 'role', 'active']
    search_fields = ['username']
# admin.site.register(Login, LoginAdmin)

class UserAdmin(admin.ModelAdmin):
    list_display = [
        'allotted_department',
    ]
    search_fields = ['first_name', 'department']

admin.site.register(Faculty, UserAdmin)
