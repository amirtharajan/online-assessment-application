from django.db import models


class Student(models.Model):

    assessment_key      =   models.CharField(max_length=32, blank=False)
    full_name           =   models.CharField(max_length=255, blank=False)
    email               =   models.CharField(max_length=255, blank=False)
    role_number         =   models.CharField(max_length=32, blank=False)
    register_number     =   models.CharField(max_length=32, blank=False)
    department          =   models.CharField(max_length=128, blank=False)
    year                =   models.IntegerField(blank=False)
    section             =   models.CharField(max_length=8, blank=False)
    attempts            =   models.IntegerField(default=1)
    attended_questions  =   models.IntegerField(default=0)
    score               =   models.IntegerField(default=0)
    is_pass             =   models.BooleanField(default=False)
    created_at          =   models.DateTimeField(auto_now_add=True)
    updated_at          =   models.DateTimeField(null=True)


    objects = models.Manager()

    class Meta:
        db_table = "students"
