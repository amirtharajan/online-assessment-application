from django.db import models

from assessment_service.models import Login



class Assessment(models.Model):

    assessment_key          =   models.CharField(max_length=32, blank=False)
    login                   =   models.ForeignKey(Login, on_delete=models.CASCADE, unique=False, null=True)
    assessment_title        =   models.CharField(max_length=255, blank=True)
    date                    =   models.DateField(blank=True, default=None)
    start_time              =   models.TimeField(blank=False)
    end_time                =   models.TimeField(blank=False)
    duration_in_minutes     =   models.IntegerField(blank=False)
    total_questions         =   models.IntegerField(blank=False)
    questions               =   models.CharField(max_length=1024, blank=True)
    total_marks             =   models.IntegerField(default=0)
    pass_mark               =   models.IntegerField(default=0)
    total_students_attended =   models.IntegerField(default=0)
    no_of_pass              =   models.IntegerField(default=0)
    no_of_fail              =   models.IntegerField(default=0)
    display_result          =   models.BooleanField(default=False)
    created_at              =   models.DateTimeField(auto_now_add=True)
    updated_at              =   models.DateTimeField(null=True)

    objects = models.Manager()


    class Meta:
        db_table = "assessments"