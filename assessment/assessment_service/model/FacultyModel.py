from django.db import models
from assessment_service.models import Login


class Faculty(models.Model):

    login                   =   models.ForeignKey(Login, on_delete=models.CASCADE, unique=False)
    allotted_department     =   models.CharField(max_length=255, blank=False)
    allotted_subject_code   =   models.CharField(max_length=32, blank=False)
    allotted_subject_name   =   models.CharField(max_length=255, blank=False)
    year                    =   models.IntegerField(blank=False)
    section                 =   models.CharField(max_length=8, blank=False)
    created_at              =   models.DateTimeField(auto_now_add=True)
    updated_at              =   models.DateTimeField(null=True)

    objects = models.Manager()

    class Meta:
        db_table = "faculty"
