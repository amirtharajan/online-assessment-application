from django.db import models
from assessment_service.models import Login
from assessment_service.model.QuestionModel import Question


class Cart(models.Model):

    login        =   models.ForeignKey(Login, on_delete=models.CASCADE, unique=False)
    question     =   models.ForeignKey(Question, on_delete=models.CASCADE, unique=False)

    objects = models.Manager()

    class Meta:
        db_table = "cart"
