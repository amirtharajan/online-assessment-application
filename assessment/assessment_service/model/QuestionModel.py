from django.db import models


class Question(models.Model):

    subject_code    =   models.CharField(max_length=32, blank=True)
    question_text   =   models.TextField(blank=True)
    question_image  =   models.FileField(upload_to='images/questions', null=True)
    choices         =   models.TextField(blank=False)
    answer          =   models.CharField(max_length=32, blank=False)
    type            =   models.CharField(max_length=32, blank=True)
    created_at      =   models.DateTimeField(auto_now_add=True)
    updated_at      =   models.DateTimeField(null=True)

    objects = models.Manager()

    class Meta:
        db_table = "questions"

