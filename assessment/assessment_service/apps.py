from django.apps import AppConfig


class UserServiceConfig(AppConfig):
    name = 'assessment_service'
